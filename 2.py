def nonConsecutiveDigits(N):
   digits = [int(d) for d in str(N)]
   digits.sort()
   new_digits = []
   for i in range(len(digits)):
       if not new_digits or new_digits[-1] != digits[i]:
           new_digits.append(digits[i])
   J = int(''.join(map(str, new_digits)))
   if J == N:
       J += 1
   return J

# TEST
print(nonConsecutiveDigits(1765))