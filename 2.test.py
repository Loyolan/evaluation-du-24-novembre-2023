import unittest

def nonConsecutiveDigits(N):
   digits = [int(d) for d in str(N)]
   digits.sort()
   new_digits = []
   for i in range(len(digits)):
       if not new_digits or new_digits[-1] != digits[i]:
           new_digits.append(digits[i])
   J = int(''.join(map(str, new_digits)))
   if J == N:
       J += 1
   return J

class TestIsValid(unittest.TestCase):
    def test_valid_html(self):
        self.assertEqual(nonConsecutiveDigits(1765), 1767)

if __name__ == '__main__':
    unittest.main()