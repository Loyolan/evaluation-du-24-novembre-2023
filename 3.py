def pairProgramming(experiences, mostExperienced):
   sorted_indices = sorted(range(len(experiences)), key=lambda k: experiences[k])
   if mostExperienced:
       return sorted_indices[-2:]
   else:
       return sorted_indices[:2]