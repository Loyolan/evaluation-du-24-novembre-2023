import unittest

def isValid(html):
    stack = []
    i = 0
    while i < len(html):
        if html[i] == '<':
            if i + 1 < len(html) and html[i + 1] == '/':
                end = html.find('>', i + 2)
                if end == -1:
                    return False
                tag = html[i+2:end]
                if not stack or stack[-1] != tag:
                    return False
                stack.pop()
                i = end + 1
            else:
                end = html.find('>', i + 1)
                if end == -1:
                    return False
                tag = html[i+1:end]
                stack.append(tag)
                i = end + 1
        else:
            i += 1
    return not stack

class TestIsValid(unittest.TestCase):
    def test_valid_html(self):
        self.assertTrue(isValid("<p><b></p>"))
        self.assertTrue(isValid("<p><span></span></p>"))
        self.assertTrue(isValid("<p></p><span></span>"))

    def test_invalid_html(self):
        self.assertFalse(isValid("<p><span></p>"))
        self.assertFalse(isValid("<p><span></p></span>"))

    def test_empty_html(self):
        self.assertTrue(isValid(""))

if __name__ == '__main__':
    unittest.main()