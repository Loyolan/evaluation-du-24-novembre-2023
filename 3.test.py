import unittest

def pairProgramming(experiences, mostExperienced):
   sorted_indices = sorted(range(len(experiences)), key=lambda k: experiences[k])
   if mostExperienced:
       return sorted_indices[-2:]
   else:
       return sorted_indices[:2]

class TestIsValid(unittest.TestCase):
    def test_pair_programming(self):
        self.assertEqual(pairProgramming([1,4,3,2,8,3], True), [1,4])
        self.assertEqual(pairProgramming([1,4,3,2,8,3], False), [0,3])
        self.assertEqual(pairProgramming([0, 3, 1, 6, 2, 3, 3, 6], True), [3,7])
if __name__ == '__main__':
    unittest.main()